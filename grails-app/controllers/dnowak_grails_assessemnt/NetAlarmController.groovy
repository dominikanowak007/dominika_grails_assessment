package dnowak_grails_assessemnt

import grails.plugin.springsecurity.SpringSecurityService
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import grails.validation.ValidationException

import javax.swing.Spring

import static org.springframework.http.HttpStatus.*

class NetAlarmController {

    def securityService

    NetAlarmService netAlarmService

   // static scaffold = NetAlarm
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond netAlarmService.list(params), model:[netAlarmCount: netAlarmService.count()]
    }

    def show(Long id) {

        respond netAlarmService.get(id)
    }

    def create() {
        respond new NetAlarm(params)
    }

    def save(NetAlarm netAlarm) {
        if (netAlarm == null) {
            notFound()
            return
        }

        try {
            netAlarmService.save(netAlarm)
        } catch (ValidationException e) {
            respond netAlarm.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'netAlarm.label', default: 'NetAlarm'), netAlarm.id])
                redirect netAlarm
            }
            '*' { respond netAlarm, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond netAlarmService.get(id)
    }

    def update(NetAlarm netAlarm) {


        if (netAlarm == null) {
            notFound()
            return
        }

        try {
            netAlarmService.save(netAlarm)
        } catch (ValidationException e) {
            respond netAlarm.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'netAlarm.label', default: 'NetAlarm'), netAlarm.id])
                redirect netAlarm
            }
            '*'{ respond netAlarm, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        netAlarmService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'netAlarm.label', default: 'NetAlarm'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'netAlarm.label', default: 'NetAlarm'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
