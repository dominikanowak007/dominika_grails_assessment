package dnowak_grails_assessemnt

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class NetworkElementController {



    NetworkElementService networkElementService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond networkElementService.list(params), model:[networkElementCount: networkElementService.count()]
    }

    def show(Long id) {
        respond networkElementService.get(id)
    }

    def create() {
        respond new NetworkElement(params)
    }

    def save(NetworkElement networkElement) {

        if (networkElement == null) {
            notFound()
            return
        }

        try {
            networkElementService.save(networkElement)
        } catch (ValidationException e) {
            respond networkElement.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'networkElement.label', default: 'NetworkElement'), networkElement.id])
                redirect networkElement
            }
            '*' { respond networkElement, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond networkElementService.get(id)
    }

    def update(NetworkElement networkElement) {
        if (networkElement == null) {
            notFound()
            return
        }

        try {
            networkElementService.save(networkElement)
        } catch (ValidationException e) {
            respond networkElement.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'networkElement.label', default: 'NetworkElement'), networkElement.id])
                redirect networkElement
            }
            '*'{ respond networkElement, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        networkElementService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'networkElement.label', default: 'NetworkElement'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'networkElement.label', default: 'NetworkElement'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
