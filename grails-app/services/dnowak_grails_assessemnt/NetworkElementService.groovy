package dnowak_grails_assessemnt

import grails.gorm.services.Service

@Service(NetworkElement)
interface NetworkElementService {

    NetworkElement get(Serializable id)

    List<NetworkElement> list(Map args)

    Long count()

    void delete(Serializable id)

    NetworkElement save(NetworkElement networkElement)

}