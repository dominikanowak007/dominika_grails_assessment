package dnowak_grails_assessemnt

import grails.gorm.services.Service

@Service(NetAlarm)
interface NetAlarmService {

    NetAlarm get(Serializable id)

    List<NetAlarm> list(Map args)

    Long count()

    void delete(Serializable id)

    NetAlarm save(NetAlarm netAlarm)

}