package dnowak_grails_assessemnt

import javax.persistence.CascadeType
import javax.persistence.FetchType
import javax.persistence.OneToMany


class NetworkElement {

    static belongsTo = [parent : NetworkElement]
    final Date createdAt = new Date()
    NetworkType networkType
    String networkName
    String identifier

    String toString(){ return ("Idenfier: "+"${identifier}" + " - " + "${networkName}") }

    //@OneToMany(fetch = FetchType.EAGER, mappedBy = "parent", cascade = CascadeType.ALL)
    static hasMany = [children : NetworkElement, alarms : NetAlarm ]

    static mapping = {

        children lazy: false
        alarms lazy: false
    }

    static constraints = {

        networkType nullable: false, inList: NetworkType.values() as List, validator: { type, networkElement ->

            if (type == NetworkType.CARRIER && networkElement.parent!=null)
            {
                return false
            }
            else if(type == NetworkType.HUB && networkElement.parent && networkElement.parent.networkType != NetworkType.CARRIER )
            {
                return false
            }
            else if(type == NetworkType.NODE && networkElement.parent.networkType != NetworkType.HUB)
            {
                return false
            }
            else if (type == NetworkType.COMPONENT && !(networkElement.parent.networkType == NetworkType.NODE  || networkElement.parent.networkType == NetworkType.COMPONENT) )
            {
                return false
            }
            else
                return true

        }
        identifier unique: true, blank: false,  nullable: false
        networkName  blank: false, validator: {val, obj, errors ->

            if(obj.id==null && obj.networkType==NetworkType.CARRIER)
            {
               if( NetworkElement.any())
                       {
                          def networkCarrierWithName  = NetworkElement.findByNetworkName(val)
                           if(networkCarrierWithName)
                               return errors.rejectValue('networkName', "${obj.getNetworkType()}" +": " + val + " already exists")
                       }
            }

             if(obj.id == null && obj.networkType != NetworkType.CARRIER && obj.parent && obj.parent.children && obj.parent.children.size() > 0 )
             {
                def allKidsElements = obj.parent.children
                def allKidsNames = allKidsElements.networkName
                if (allKidsNames.contains(val))
                {
                    return errors.rejectValue('networkName', "${obj.getNetworkType()}" + ": " + val + " already exists")
                }
            }
        }

        parent nullable: true, validator: {val, obj ->
            if (obj && obj.id==null && obj.networkType == NetworkType.CARRIER && val==null) return true
            else if (obj && obj.networkType != NetworkType.CARRIER && val==null) return false

        }
        children nullable: true
        alarms nullable: true, validator: {val, obj -> if(obj && obj.id==null&&obj.networkType == NetworkType.CARRIER && val!=null)return false }


    }
}
