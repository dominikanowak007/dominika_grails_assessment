package dnowak_grails_assessemnt

enum NetworkType {
    CARRIER(1),
    HUB(2),
    NODE(3),
    COMPONENT(4)

    private final int level

    NetworkType(int value) {
        this.level = value
    }

    int getLevel() {
        level
    }

    String toString() {
        return name()
    }
}


