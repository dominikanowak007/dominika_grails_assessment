package dnowak_grails_assessemnt

import java.time.LocalDateTime


class Ticket {

    String timestamp = LocalDateTime.now().toString()
    boolean autogenerated = false
    User createdBy


    @Override
    String toString() {
        return "${id}" + " - " + timestamp
    }
    static belongsTo = [

            alarm : NetAlarm
    ]


    static constraints = {
        alarm nullable: false, unique: true
        createdBy nullable: true, validator: { val, obj -> if(!obj.autogenerated && obj.createdBy == User.first()) return false}
        timestamp nullable: false

    }


}