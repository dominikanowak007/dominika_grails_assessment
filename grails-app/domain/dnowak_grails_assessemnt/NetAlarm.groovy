package dnowak_grails_assessemnt

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

import java.time.*

import static dnowak_grails_assessemnt.User.currentUser

class NetAlarm {

    static belongsTo = [networkElement: NetworkElement]
    String alarmStatus
    Ticket ticket
    Boolean active = (alarmStatus==AlarmStatus.CLEARED ? false : true)
    String createdByUser = SecurityContextHolder.getContext().getAuthentication()!=null ? SecurityContextHolder.getContext().getAuthentication().getName(): null
    final LocalDateTime timestamp = LocalDateTime.now()
    String createdAt =  timestamp.toString()


    static hasOne = [ticket: Ticket]

    /*String toString(){
       String isActive = active ? "Active": "Not Active"
       def status = " ${networkElement.networkName}" + " - " + "${alarmStatus}" +" - "+ isActive
        return status
    }*/

    static constraints = {
        networkElement nullable: false, validator: { val, obj -> if (obj.networkElement.networkType == NetworkType.CARRIER) return false   }
        alarmStatus unique: true, inList:  [AlarmStatus.MINOR.toString(), AlarmStatus.CRITICAL.toString(), AlarmStatus.WARNING.toString(), AlarmStatus.CLEARED.toString()]
        active display : false
        ticket nullable: true
        createdByUser nullable:true
        timestamp  display:false
    }
}