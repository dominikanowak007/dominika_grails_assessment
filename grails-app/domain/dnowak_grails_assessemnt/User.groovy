package dnowak_grails_assessemnt

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

import javax.persistence.FetchType
import javax.persistence.OneToMany

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired


 static hasMany = [ticketsCreated:Ticket]

   Set<Role> getAuthorities() {
       (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
   }


    static constraints = {
        username nullable: false, blank: false, unique: true
        password nullable: false, blank: false, password: true

    }

    static mapping = {
	    password column: '`password`'
        authorities lazy: false
    }

}
