package dnowak_grails_assessemnt

enum AlarmStatus {

    CRITICAL,
    MINOR,
    WARNING,
    CLEARED
}