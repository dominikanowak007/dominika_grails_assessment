package dnowak_grails_assessemnt

//import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->

        def userRole = Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER', ).save(flush: true)
        def adminRole = Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN', ).save(flush: true)
        def autoRole = new Role(authority: 'ROLE_TICKETER').save(flush: true)
        def superRole = new Role(authority: 'ROLE_SUPERMAN').save(flush:true)

        def autoUser =  new User( username: 'Ticketer', enabled: true, password: 'Ticket007')
        def adminUser = new User(username: 'Dominika', enabled: true, password: 'Ropucha007')
        def nocUser = new User(username: 'NOC', enabled: true, password: 'Ropucha0072')
        def superUser = new User(username: 'Superman', enabled: true, password: 'Super007')

        autoUser.save(flush:true)
        adminUser.save(flush: true)
        nocUser.save(flush: true)
        superUser.save(flush:true)

        UserRole.create( autoUser, autoRole, true)
        UserRole.create (adminUser, adminRole, true)
        UserRole.create( nocUser, userRole, true)
        UserRole.create( superUser, superRole, true)

        UserRole.withSession {
            it.flush()
            it.clear()
        }
        println"//////////////////////////////////////////////////////////////////////"
        println(User.findAll())
        println"///////////////////////////////////////////////////////////////////////"
        println(Role.findAll())
        println"///////////////////////////////////////////////////////////////////////"
        println(UserRole.findAll())
        println"///////////////////////////////////////////////////////////////////////"

        NetworkElement carrier = new NetworkElement( identifier: 1, networkType: NetworkType.CARRIER, networkName: 'MyFirstCarrier')
        carrier.save()
        NetworkElement hub = new NetworkElement( identifier: 2, networkType: NetworkType.HUB, networkName: 'MyFirstHub', parent: NetworkElement.first())
        hub.save()
    }
    def destroy = {
    }
}
