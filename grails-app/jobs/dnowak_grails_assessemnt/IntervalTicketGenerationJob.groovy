package dnowak_grails_assessemnt

import java.time.*


class IntervalTicketGenerationJob {
    static triggers = {
                simple  startDelay:60000, //execute 240 sec after app starts
                        repeatInterval: 60000l // execute job once in 60 seconds
    }

    def execute()
    {
        println "Auto Ticket Generation is running now..."

        List<NetAlarm> alarmList = NetAlarm.getAll()
        if(alarmList)
        {
            for (NetAlarm alarm in alarmList)
             {
                if (alarm.alarmStatus != AlarmStatus.CLEARED && alarm.active && !alarm.ticket)
                {
                    LocalDateTime timestamp = alarm.timestamp
                    LocalDateTime timeGenerationPoint = timestamp.plusSeconds(30)

                       if (alarm.timestamp < timeGenerationPoint)
                       {
                          Ticket generatedTicket = new Ticket(
                             alarm: alarm,
                             autogenerated: true,
                             user: User.first() )

                             if( generatedTicket.save(flush:true))  println "New ticket saved"
                       }
                       else println 'NO New Tickets '
                }
                 else println 'No Tickets generated - No Active Alarms listed'
             }
        }
    }
}
