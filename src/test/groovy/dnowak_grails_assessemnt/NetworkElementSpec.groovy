package dnowak_grails_assessemnt

import grails.testing.gorm.DomainUnitTest
import spock.lang.Ignore
import spock.lang.Specification

import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertFalse
import static junit.framework.Assert.assertNotSame
import static junit.framework.Assert.assertTrue

class NetworkElementSpec extends Specification implements DomainUnitTest<NetworkElement> {

    def setup() {


    }


    def cleanup() {}

    void "test Network Element Carrier persistence"() {

        setup:
        new NetworkElement(identifier: 11, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save()
        new NetworkElement(identifier: 22, networkName: 'Test Carrier2', networkType: NetworkType.CARRIER).save()

        expect:
        def testCarriers = NetworkElement.findAll { netelement -> netelement.properties.networkType == NetworkType.CARRIER }
        println testCarriers
    }

    void "test Network Element identifier cannot be null"() {

        when:
        domain.identifier = null
        then:
        !domain.validate(['identifier'])
        domain.errors['identifier'].code == 'nullable'
    }

    void "test Network Element children are nullable"()
    {
        when:
        domain.children = null
        then:
        def noChildren=domain.validate(['children'])
        assertEquals(noChildren,true)
    }

    void 'test Network Element identifier cannot be  cannot be blank'() {
        when:
        domain.identifier = ''

        then:
        !domain.validate(['identifier'])
        domain.errors['identifier'].code == 'blank'
    }

    void "test Network Element Network Name cannot be null"() {

        when:
        domain.networkName = null
        then:
        !domain.validate(['networkName'])
        domain.errors['networkName'].code == 'nullable'
    }

    void "test Network Element Network Type cannot be null"() {

        when:
        domain.networkType = null
        then:
        !domain.validate(['networkType'])
        domain.errors['networkType'].code == 'nullable'
    }

    void 'test Network Element Network Name cannot be cannot be blank'() {
        when:
        domain.networkName = ''

        then:
        !domain.validate(['networkName'])
        domain.errors['networkName'].code == 'blank'
    }

    void 'test Network Carrier Parent can be null '() {
        when:
        domain.networkType = NetworkType.CARRIER
        domain.parent = null

        then:
        def parentValidated = domain.validate(['parent'])
        assertTrue(parentValidated)

    }

    void 'test Network Hub Parent cannot be null'() {
        when:
        def child = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: null, networkName: 'my Hub')
        then:
        def parentValidated = child.validate(['parent'])
        assertFalse(parentValidated)
    }

    void 'test Network Element Identifier of Type: Carrier must be unique '() {

        when: 'First Network Element with identifier = 100'
        def netElement1 = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'Test Carrier 1')
        and: 'First element is saved'
        def validated1= netElement1.validate()
        netElement1.save()
        and: 'One Net Element exists'
        NetworkElement.count() == 1
        and: 'New Network Element with the same identifier=100'
        def netElement2 = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'Test Carrier 2')
        then: 'Second Network Element with the same identifier like First Network Element is not valid'
        def validated = netElement2.validate(['identifier'])
        then:
        assertEquals(validated,false )
    }

    void 'test Network Element identifier is unique for  different network types '() {

        when: 'Parent Network Element Carrier with identifier = 101'

        def parent = new NetworkElement(identifier: 101, networkType: NetworkType.CARRIER, networkName: 'Test Carrier')

        then: 'First element is valid'
        parent.validate()
        and: 'first element is saved'
        parent.save()
        and: 'Carrier exists'
        NetworkElement.count() == 1
        when: 'Child with the same identifier = 101'
        def child = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: parent, networkName: 'Test Hub')
        then: 'Second Network Element with the same identifier like parent is not valid'
        !child.validate(['identifier'])
        and: 'Unique Error is populated'
        child.errors['identifier']?.code == 'unique'
        and: "saving fails"
        !child.save()
        and: 'Second Element was not added'
        NetworkElement.count() == 1
    }

    void 'test Network Element Type cannot be null'() {

        when:
        def parent = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'First').save()
        def child = new NetworkElement(identifier: 101, networkType: null, parent: parent, networkName: 'my Hub')

        then:
        def networkTypeisValidated = child.validate(['networkType'])
        assertEquals(networkTypeisValidated, false)
    }

    void 'test Network Element Type of enum Type'() {

        when:
        def parent = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'First').save()
        def child = new NetworkElement(identifier: 101, networkType: 'no type', parent: parent, networkName: 'my Hub')

        then:
        def networkTypeisValidated = child.validate(['networkType'])
        assertEquals(networkTypeisValidated, false)
    }

    void 'test Network Element Type hierarchy not in order '() {

        when:
        def parent = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'First').save()
        def child = new NetworkElement(identifier: 101, networkType: NetworkType.NODE, parent: parent, networkName: 'my Hub')

        then:
        def networkTypeisValidated = child.validate(['networkType'])
        assertEquals(networkTypeisValidated, false)
    }

    void 'test Network Element of Type Component can have parent of type Component '() {

        when:
        def greatGran = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'FirstCarrier').save()
        def grandad = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: greatGran, networkName: 'FirstHub')
        def parent = new NetworkElement(identifier: 102, networkType: NetworkType.NODE, parent: grandad, networkName: 'FirstNode')
        def child = new NetworkElement(identifier: 103, networkType: NetworkType.COMPONENT, parent: parent, networkName: 'FirstLevelComponent')
        def grandChild= new NetworkElement(identifier: 102, networkType: NetworkType.COMPONENT, parent: child, networkName: 'SecondLevelComponent')
        then:
        def secondLevelComponentisValidated = grandChild.validate(['networkType'])
        assertEquals(secondLevelComponentisValidated, true)
    }

    void 'test Network Element Type hierarchy in order'() {

        when:
        def grandad = new NetworkElement(identifier: 10, networkType: NetworkType.CARRIER, networkName: 'c').save()
        def parent = new NetworkElement(identifier: 100, networkType: NetworkType.HUB, networkName: 'h', parent: grandad).save()
        def child = new NetworkElement(identifier: 101, networkType: NetworkType.NODE, parent: parent, networkName: 'n')

        then:
        def networkTypeisValidated = child.validate(['networkType'])
        assertEquals(networkTypeisValidated, true)
    }

    void 'test Network Element Name is unique for the same types'() {

        when: 'Network Element Carrier with Name = First'
        def firstCarrier = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'First')
        then: 'First element is valid'
        firstCarrier.validate()
        and: 'first element is saved'
        firstCarrier.save()
        and: 'Carrier exists'
        NetworkElement.count() == 1
        when: 'Network Element of the same type: Carrier with the same Name = First'
        def secondCarrier = new NetworkElement(identifier: 101, networkType: NetworkType.CARRIER, networkName: 'First')
        then: 'Second Network Element with the same identifier like parent is not valid'
        def secondCarrierisValidated=domain.validate(['networkName'])
        def expected = false
        assertEquals(secondCarrierisValidated, expected)
    }

    void 'test Network Element Name can be the same for the different types'() {

        when: 'Network Element Carrier with Name = Kaczka'
        def firstCarrier = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'Kaczka')
        and: 'first element is saved'
        firstCarrier.save()
        and:
        def countBefore=NetworkElement.count()
        and: 'Child with the same Name = Kaczka'
        def hub = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: firstCarrier, networkName: 'Kaczka')
        and:
        hub.save()
        then:
        def countAfter = NetworkElement.count()
        assertNotSame(countBefore,countAfter)
    }

    void 'test the same name for children of the same parent '()
    {
        when: 'Network Element Hub with Name = Identical on network carrier'
        def carrier = new NetworkElement(identifier: 100, networkType: NetworkType.CARRIER, networkName: 'Kaczka')
        and:
        carrier.save()
        def hub1 = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: carrier, networkName: 'Kaczka')
        and:
        hub1.save()
        and:
        def countBefore=NetworkElement.count()
        and: 'Second Hub with the same Name = Kaczka'
        def hub2 = new NetworkElement(identifier: 101, networkType: NetworkType.HUB, parent: carrier, networkName: 'Kaczka')
        and:
        hub2.save()
        then:
        def countAfter = NetworkElement.count()
        assert(countBefore==countAfter)
    }

    void 'Network Element children are nullable'()
    {
        when:
        domain.children = null
        then:
        def childrenValidated=domain.validate(['children'])
        assertEquals(childrenValidated, true)
        def errorsOnChildren = domain.errors['identifier']
        assertEquals(errorsOnChildren, null)
    }

}