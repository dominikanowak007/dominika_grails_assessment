package dnowak_grails_assessemnt

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import grails.validation.ValidationException
import spock.lang.*

class NetAlarmControllerSpec extends Specification implements ControllerUnitTest<NetAlarmController>, DomainUnitTest<NetAlarm> {

    def populateValidParams(params) {


        def carrier = new NetworkElement(identifier: 11, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 111, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmStatusType = AlarmStatus.WARNING.toString()
        def creator= null
        params=[:]
        params["alarmStatus"] = alarmStatusType
        params.createdByUser = creator
        params.networkElement = hub

        assert params != null
        return params

    }

    void "Test the index action returns the correct model"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * list(_) >> []
            1 * count() >> 0
        }

        when:"The index action is executed"
        controller.index()

        then:"The model is correct"
        !model.netAlarmList
        model.netAlarmCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
        controller.create()

        then:"The model is correctly created"
        model.netAlarm!= null
    }

    void "Test the save action with a null instance"() {
        when:"Save is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        controller.save(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/netAlarm/index'
        flash.message != null
    }

    void "Test the save action correctly persists"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * save(_ as NetAlarm)
        }

        when:"The save action is executed with a valid instance"
        response.reset()
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def params=populateValidParams()
        def netAlarm = new NetAlarm(params)
        netAlarm.id = 1

        controller.save(netAlarm)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/netAlarm/show/1'
        controller.flash.message != null
    }

    void "Test the save action with an invalid instance"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * save(_ as NetAlarm) >> { NetAlarm netAlarm ->
                throw new ValidationException("Invalid instance", netAlarm.errors)
            }
        }

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def netAlarm = new NetAlarm()
        controller.save(netAlarm)

        then:"The create view is rendered again with the correct model"
        model.netAlarm != null
        view == 'create'
    }

    void "Test the show action with a null id"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * get(null) >> null
        }

        when:"The show action is executed with a null domain"
        controller.show(null)

        then:"A 404 error is returned"
        response.status == 404
    }

    void "Test the show action with a valid id"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * get(2) >> new NetAlarm()
        }

        when:"A domain instance is passed to the show action"
        controller.show(2)

        then:"A model is populated containing the domain instance"
        model.netAlarm instanceof NetAlarm
    }

    void "Test the edit action with a null id"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * get(null) >> null
        }

        when:"The show action is executed with a null domain"
        controller.edit(null)

        then:"A 404 error is returned"
        response.status == 404
    }

    void "Test the edit action with a valid id"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * get(2) >> new NetAlarm()
        }

        when:"A domain instance is passed to the show action"
        controller.edit(2)

        then:"A model is populated containing the domain instance"
        model.netAlarm instanceof NetAlarm
    }


    void "Test the update action with a null instance"() {
        when:"Save is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/netAlarm/index'
        flash.message != null
    }

    void "Test the update action correctly persists"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * save(_ as NetAlarm)
        }

        when:"The save action is executed with a valid instance"
        response.reset()
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        populateValidParams(params)
        def netAlarm = new NetAlarm(params)
        netAlarm.id = 1

        controller.update(netAlarm)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/netAlarm/show/1'
        controller.flash.message != null
    }

    void "Test the update action with an invalid instance"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * save(_ as NetAlarm) >> { NetAlarm netAlarm ->
                throw new ValidationException("Invalid instance", netAlarm.errors)
            }
        }

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(new NetAlarm())

        then:"The edit view is rendered again with the correct model"
        model.netAlarm != null
        view == 'edit'
    }

    void "Test the delete action with a null instance"() {
        when:"The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then:"A 404 is returned"
        response.redirectedUrl == '/netAlarm/index'
        flash.message != null
    }

    void "Test the delete action with an instance"() {
        given:
        controller.netAlarmService = Mock(NetAlarmService) {
            1 * delete(2)
        }

        when:"The domain instance is passed to the delete action"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(2)

        then:"The user is redirected to index"
        response.redirectedUrl == '/netAlarm/index'
        flash.message != null
    }
}






