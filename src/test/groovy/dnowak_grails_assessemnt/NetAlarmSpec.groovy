package dnowak_grails_assessemnt

import grails.testing.gorm.DomainUnitTest

import spock.lang.Specification




class NetAlarmSpec extends Specification implements DomainUnitTest<NetAlarm> {


    def setup() { }

    def cleanup() {}

   void "test Network Alarm persistence"() {

        setup:
        def carrier=new NetworkElement(identifier: 11, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 111, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmOnHub = new NetAlarm(alarmStatus: AlarmStatus.WARNING.toString(), networkElement: hub, createdByUser: null).save(flush:true)

        expect:
        def testAlarm = NetworkElement.list().findAll{n->n.networkType != NetworkType.CARRIER && n.alarms!=null}
        assert( testAlarm.size()==1)
    }
    void "test Network Alarm unique "() {

        setup:
        def carrier=new NetworkElement(identifier: 11, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 111, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmOnHub1 = new NetAlarm(alarmStatus: AlarmStatus.WARNING.toString(), networkElement: hub, createdByUser: null).save(flush:true)
        def alarmOnHub2 = new NetAlarm(alarmStatus: AlarmStatus.WARNING.toString(), networkElement: hub, createdByUser: null)
        expect:
        def duplicateAlarmRejected = !alarmOnHub2.validate()
        assert(duplicateAlarmRejected==true)

    }

    void 'muliple Alarm of different status allowed'()
    {
        setup:
        def carrier=new NetworkElement(identifier: 18, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 118, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmOnHub1 = new NetAlarm(alarmStatus: AlarmStatus.WARNING.toString(), networkElement: hub, createdByUser: null).save(flush:true)
        def alarmOnHub2 = new NetAlarm(alarmStatus: AlarmStatus.MINOR.toString(), networkElement: hub, createdByUser: null)
        expect:
        def alarmsDifferentStatusAllowed = alarmOnHub2.validate()
        assert(alarmsDifferentStatusAllowed==true)

    }

    void 'Alarm must be set on element of the Network '()
    {
        when:
        domain.networkElement = null
        then:
        !domain.validate(['networkElement'])
        domain.errors['networkElement'].code == 'nullable'

    }
    void 'Alarm status must be from closed list'()
    {
        when:
        domain.alarmStatus = 'Bla'
        then:
        !domain.validate(['alarmStatus'])
        domain.errors['alarmStatus'].code == 'not.inList'
    }
    void 'Alarm ticket is nullable'()
    {
        when:
        domain.ticket = null
        then:
        def ticketIsNullable = domain.validate(['ticket'])
        assert (ticketIsNullable == true)
    }

    void 'Alarm creator is not nullable'()
    {
        when:
        domain.createdByUser = null
        then:
        def validated=domain.validate(['createdByUser'])
        assert(validated)

    }

}
