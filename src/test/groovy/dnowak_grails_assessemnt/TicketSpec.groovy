package dnowak_grails_assessemnt

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class TicketSpec extends Specification implements DomainUnitTest<Ticket> {

    def setup() { }

    def cleanup() { }

    void "test Ticket has one Alarm"() {

        when:
        def carrier = new NetworkElement(identifier: 158, networkName: 'Test Carrier', networkType: NetworkType.CARRIER)
                carrier.save(flush:true)
        def hub = new NetworkElement(identifier: 1168, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmOnHub1 = new NetAlarm(alarmStatus: AlarmStatus.WARNING.toString(), networkElement: hub, createdByUser: 'me').save(flush:true)
        def ticket1 = new Ticket(alarm:alarmOnHub1, createBy: new User(username: "bla", password: "bla")).save()
        def ticket2= new Ticket(alarm:alarmOnHub1, createBy: new User(username: "bla", password: "bla"))
        and:
        def oneTicketPerAlarmOnly=!ticket2.validate()

        then:
        assert(oneTicketPerAlarmOnly)


    }

    void 'test Ticket alarm property is not nullable'()
    {
        when:
        domain.alarm = null
        then:
        !domain.validate(['alarm'])
        domain.errors['alarm'].code == 'nullable'
    }

    void 'test Ticket Createdby property cannot be null'()
    {
        when:
        domain.createdBy = null
        then:
        def userMustBeValid=!domain.validate(['createdBy'])
        assert(userMustBeValid==true)
    }
    void 'test Ticket Createdby property must be valid user'()
    {
        when:
        domain.createdBy= new User(username: 'Test', password: 'secret')
        then:
        def userMustBeValid=domain.validate(['createdBy'])
        assert(userMustBeValid)
    }


}
