package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class NetAlarmServiceSpec extends Specification {

    NetAlarmService netAlarmService
    SessionFactory sessionFactory

    private Long setupData() {

        def carrier = new NetworkElement(identifier: 11, networkName: 'Test Carrier', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 111, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarmStatusType1 = AlarmStatus.WARNING.toString()
        def alarmStatusType2 = AlarmStatus.MINOR.toString()
        def alarmStatusType3 = AlarmStatus.CRITICAL.toString()
        def alarmStatusType4 = AlarmStatus.CLEARED.toString()

        new NetAlarm(networkElement: hub, alarmStatus:alarmStatusType1).save(flush: true, failOnError: true)
        new NetAlarm(networkElement: hub, alarmStatus:alarmStatusType2).save(flush: true, failOnError: true)
        NetAlarm netAlarm = new NetAlarm(networkElement: hub, alarmStatus:alarmStatusType3).save(flush: true, failOnError: true )
        new NetAlarm(networkElement: hub, alarmStatus:alarmStatusType4).save(flush: true, failOnError: true)


        return netAlarm.id
    }

    void "test get"() {
        setupData()

        expect:
        netAlarmService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<NetAlarm> netAlarmList = netAlarmService.list(max: 2, offset: 2)

        then:
        netAlarmList.size() == 2
         assert (netAlarmList.first().alarmStatus == AlarmStatus.CRITICAL.toString())
    }

    void "test count"() {
        setupData()

        expect:
        netAlarmService.count() == 4
    }

    void "test delete"() {
        Long netAlarmId = setupData()

        expect:
        netAlarmService.count() == 4

        when:
        netAlarmService.delete(netAlarmId)
        sessionFactory.currentSession.flush()

        then:
        netAlarmService.count() == 3
    }

    void "test save"() {
        when:

        def carrier = new NetworkElement(identifier: 10, networkName: 'TestC', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 110, networkName: 'TestH', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def node = new NetworkElement(identifier: 1100, networkName: 'TestN', networkType: NetworkType.NODE, parent: hub).save(flush:true)
        NetAlarm netAlarm = new NetAlarm(networkElement: node, alarmStatus: AlarmStatus.CLEARED)

        netAlarmService.save(netAlarm)

        then:
        netAlarm.id != null
    }
}
