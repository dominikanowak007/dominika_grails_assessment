package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory
import sun.nio.ch.Net

@Integration
@Rollback
class NetworkElementServiceSpec extends Specification {

    NetworkElementService networkElementService
    SessionFactory sessionFactory

    private Long setupData() {

        NetworkElement carrier= new NetworkElement(identifier: 1000, networkType: NetworkType.CARRIER,networkName: 'Lucky Carrier').save(flush: true, failOnError: true)
        NetworkElement hub1 = new NetworkElement(identifier: 1001, networkType: NetworkType.HUB, networkName: 'Lucky Hub', parent:carrier ).save(flush: true, failOnError: true)
        NetworkElement hub2 = new NetworkElement(identifier: 1002, networkType: NetworkType.HUB, networkName: 'Not So Lucky Hub', parent:carrier).save(flush: true, failOnError: true)
        NetworkElement node1 = new NetworkElement(identifier: 1003, networkType: NetworkType.NODE, networkName: 'Lucky Node', parent: hub1).save(flush: true, failOnError: true)
        NetworkElement node2 = new NetworkElement(identifier: 1004, networkType: NetworkType.NODE, networkName: 'Lucky Node', parent:hub2).save(flush: true, failOnError: true)
        NetworkElement component = new NetworkElement(identifier: 1005, networkType: NetworkType.COMPONENT, networkName: 'Lucky Component', parent: node1).save(flush: true, failOnError: true)

        return node2.id
    }

    void "test get"() {
        setupData()

        expect:
        networkElementService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<NetworkElement> networkElementList = networkElementService.list(max: 2, offset: 2)

        then:
        networkElementList.size() == 2
        assert(networkElementList.first().networkType==NetworkType.CARRIER && networkElementList.last().networkType == NetworkType.HUB)
    }

    void "test count"() {
        setupData()

        expect:
        def count = networkElementService.count()
        count == 8
    }

    void "test delete"() {
        Long networkElementId = setupData()

        expect:
        networkElementService.count() == 8

        when:
        networkElementService.delete(networkElementId)
        sessionFactory.currentSession.flush()

        then:
        networkElementService.count() == 7
    }

    void "test save"() {
        when:

        NetworkElement networkElement = new NetworkElement(identifier: 1009, networkType: NetworkType.CARRIER, networkName: 'Lucky Carriery')
        networkElementService.save(networkElement)

        then:
        networkElement.id != null
    }
}
