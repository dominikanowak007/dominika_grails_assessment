package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class UserServiceSpec extends Specification {

    UserService userService
    SessionFactory sessionFactory

    private Long setupData() {

        new User(username: 'user1',password: 'pass1').save(flush: true, failOnError: true)
        new User(username: 'user2',password: 'pass2').save(flush: true, failOnError: true)
        User user = new User(username: 'user3',password: 'pass3').save(flush: true, failOnError: true)
        new User(username: 'user4',password: 'pass4').save(flush: true, failOnError: true)
        new User(username: 'us5',password: 'pass5').save(flush: true, failOnError: true)

        return user.id
    }

    void "test get"() {
        setupData()

        expect:
        userService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<User> userList = userService.list(max: 2, offset: 6)

        then:
        userList.size() == 2
        assert userList.first().username == 'user3' && userList.last().username == 'user4'
    }

    void "test count"() {
        setupData()

        expect:
        def searchedUsers = userService.list().findAll {u->u.username.contains('user')}
        searchedUsers.size() == 4
    }

    void "test delete"() {
        Long userId = setupData()

        expect:
        userService.count() == 9

        when:
        userService.delete(userId)
        sessionFactory.currentSession.flush()

        then:
        userService.count() == 8
    }

    void "test save"() {
        when:
        User newUser = new User(username: 'Spoocky', password: 'Ameba')

        userService.save(newUser)

        then:
        newUser.id != null
    }
}
