package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class RoleServiceSpec extends Specification {

    RoleService roleService
    SessionFactory sessionFactory

    private Long setupData() {

        new Role(authority: 'ROLE_CA').save(flush: true, failOnError: true)
        new Role(authority: 'ROLE_PL').save(flush: true, failOnError: true)
        Role role = new Role(authority: 'SUPERWOMEN').save(flush: true, failOnError: true)
        new Role(authority: 'ROLE_PR').save(flush: true, failOnError: true)
        new Role(authority: 'ROLE_VIP').save(flush: true, failOnError: true)

         return role.id
    }

    void "test get"() {
        setupData()

        expect:
        roleService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Role> roleList = roleService.list(max: 2, offset: 2)

        then:
        roleList.size() == 2
        assert (roleList.last().authority=='ROLE_SUPERMAN')
    }

    void "test count"() {
        setupData()

        expect:
        roleService.count() == 9
    }

    void "test delete"() {
        Long roleId = setupData()

        expect:
        roleService.count() == 9

        when:
        roleService.delete(roleId)
        sessionFactory.currentSession.flush()

        then:
        roleService.count() == 8
    }

    void "test save"() {
        when:

        Role role = new Role(authority: 'ROLE_EXTRA')
        roleService.save(role)

        then:
        role.id != null
    }
}
