package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class UserRoleServiceSpec extends Specification {

    UserRoleService userRoleService
    SessionFactory sessionFactory

    private UserRole setupData() {

        User user = new User(username: 'DummyUser', password: 'dummypassword09').save(flush: true, failOnError: true)

        Role role1 = new Role(authority: 'ROLE_VIP').save(flush: true, failOnError: true)
        Role role2 = new Role(authority: 'ROLE_UN').save(flush: true, failOnError: true)
        new UserRole(user: user, role:role1).save(flush: true, failOnError: true)
        new UserRole(user:user, role:role2).save(flush: true, failOnError: true)

        Role role3 = new Role(authority: 'ROLE_AUTHOR').save(flush: true, failOnError: true)
        UserRole userRole = new UserRole(user:user, role:role3).save(flush: true, failOnError: true)

        return userRole
    }

    void "test get"() {
       def userRole = setupData()

        expect:
        userRoleService.get(userRole) != null
    }

    void "test list"() {
        setupData()

        when:
        List<UserRole> userRoleList = userRoleService.list(max: 2, offset: 5)

        then:
        userRoleList.size() == 2
        assert userRoleList.first().role.authority == 'ROLE_UN' && userRoleList.last().user.username=='DummyUser'

    }

    void "test count"() {
        setupData()

        expect:
        def dummyRoles = userRoleService.list().findAll{ur->ur.user.username=='DummyUser'}
        def dummyRolesCount= dummyRoles.size()
        assert dummyRolesCount==3
    }

    void "test delete"() {
        def dummyUser = setupData()

        expect:
        userRoleService.count() == 7

        when:
        userRoleService.delete(dummyUser)
        sessionFactory.currentSession.flush()

        then:
        userRoleService.count() == 6
    }

    void "test save"() {

        when:
        User user = new User(username: 'testUser',password: 'testPass').save(flush:true)
        Role role= new Role(authority: 'ROLE_R').save(flush:true)

        UserRole userRole = new UserRole(user:user, role:role)

        userRoleService.save(userRole)

        then:
        userRole.role != null && userRole.user != null
    }
}
