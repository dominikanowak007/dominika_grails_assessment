package dnowak_grails_assessemnt

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TicketServiceSpec extends Specification {

    TicketService ticketService
    SessionFactory sessionFactory

    private Long setupData() {

        def carrier = new NetworkElement(identifier: 158, networkName: 'Test Carrier', networkType: NetworkType.CARRIER)
        carrier.save(flush:true)
        def hub = new NetworkElement(identifier: 1168, networkName: 'Test Hub', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def hub2 = new NetworkElement(identifier: 1178, networkName: 'Test Hub2', networkType: NetworkType.HUB, parent: carrier).save(flush:true)
        def alarm1 = new NetAlarm(alarmStatus: AlarmStatus.WARNING, networkElement: hub, createdByUser: 'me').save(flush:true)
        def alarm2 =  new NetAlarm(alarmStatus: AlarmStatus.MINOR, networkElement: hub, createdByUser: 'me').save(flush:true)
        def alarm3 =  new NetAlarm(alarmStatus: AlarmStatus.CLEARED, networkElement: hub, createdByUser: 'me').save(flush:true)
        def alarm4 =  new NetAlarm(alarmStatus: AlarmStatus.CRITICAL, networkElement: hub2, createdByUser: 'me').save(flush:true)
        new Ticket(alarm: alarm1).save(flush: true, failOnError: true)
        new Ticket(alarm: alarm2).save(flush: true, failOnError: true)
        new Ticket(alarm: alarm3).save(flush: true, failOnError: true)
        Ticket ticket = new Ticket(alarm: alarm4).save(flush: true, failOnError: true)

      return  ticket.id
    }

    void "test get"() {
        setupData()

        expect:
        ticketService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Ticket> ticketList = ticketService.list(max: 2, offset: 2)

        then:
        ticketList.size() == 2
       assert ticketList.first().alarm.alarmStatus.toString() == AlarmStatus.CLEARED.toString() && ticketList.last().alarm.alarmStatus ==AlarmStatus.CRITICAL.toString()
    }

    void "test count"() {
        setupData()

        expect:
        ticketService.count() == 4
    }

/*   void "test delete"() {
        Long ticketId = setupData()

        expect:
        ticketService.count() == 4
        def ticketToBeDeleted = ticketService.list().find{t->t.id==ticketId}
        def correspondingAlarm = ticketToBeDeleted.alarm
        when:


        def nope = ticketService.delete(ticketId)
        sessionFactory.currentSession.flush()

        then:
        def countAfter = ticketService.count()
        countAfter != 4

    }*/

    void "test save"() {
        when:

        def carrier = new NetworkElement(identifier: 19, networkName: 'C2', networkType: NetworkType.CARRIER).save(flush:true)
        def hub = new NetworkElement(identifier: 119, networkName: 'H2', networkType: NetworkType.HUB,parent: carrier).save(flush:true)
        def alarm = new NetAlarm(alarmStatus: AlarmStatus.WARNING, networkElement: hub, createdByUser: 'me').save(flush:true)
        Ticket ticket = new Ticket(alarm: alarm)
        ticketService.save(ticket)

        then:
        ticket.id != null
    }
}
